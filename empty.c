/******************************************************************************/
/*
  Sin argumentos ni variables de entorno
  ./programa
*/

int main(void)
{
  return 0;
}

/******************************************************************************/
/*
  Con argumentos, sin variables de entorno
  ./programa arg1 arg2 arg3
*/

int main(int argc, char* argv[])
{
  printf("Línea de comandos, argumentos: %d\n", argc);
  for (int i=0 ; i < argc ; i++)
    printf("\targv[%d]:	%s\n", i, argv[i]);

  return 0;
}

/******************************************************************************/
/*
  Con argumentos y variables de entorno
  VARIABLE=valor ./programa arg1 arg2 arg3
*/

int main(int argc, char* argv[], char* envp[])
{
  printf("Línea de comandos, argumentos: %d\n", argc);
  for (int i=0 ; i < argc ; i++)
    printf("\targv[%d]:	%s\n", i, argv[i]);

  printf("Variables de entorno:\n");
  for (int i=0 ; envp[i] != NULL ; i++)
    printf("\tenvp[%d]:	%s\n", i, envp[i]);

  return 0;
}
