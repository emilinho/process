#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>

/* Define constantes globales */
#define SLEEP
#define ARCHIVO "/tmp/unix.sock"
#define BUF_LEN 255
#define BACKLOG 5

/* Caracter de fin de transmisión */
char EOT[] = { 0x04, 0x00 };

/* Mensaje a imprimir */
char *MENSAJE = "aeiou";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyz";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

/* Imprime un mensaje de error y sale el programa */
void error(char * msg)
{
  perror(msg);
  exit(EXIT_FAILURE);
}

/* Función principal */
int main(void)
{
  /* Descriptores de archivo para sockets */
  int server_sock_fd, client_sock_fd;
  /* Bytes leídos del socket*/
  ssize_t bytes;
  /* Estructuras de socket */
  struct sockaddr_un server_addr, client_addr;
  /* Tamaño de las estructuras del socket */
  socklen_t server_len, client_len;

  /* Crea un proceso hijo */
  pid_t pid = fork();

  /* Error en la creación del proceso hijo */
  if (pid < 0)
    error("Error en fork(2)");

  /* Código que ejecuta el proceso padre */
  if (pid > 0)
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getpid();
    pid_t pid_hijo = pid;

    /* Inicializa el búffer con '\0' */
    char buffer[BUF_LEN] = "";
    bzero(buffer, BUF_LEN);

    /* Borra el archivo si existe */
    struct stat sb;
    if(stat(ARCHIVO, &sb) == 0)
      unlink(ARCHIVO);

    /* Crea el socket del servidor y obtiene el descriptor de archivos */
    if ((server_sock_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
      error("[padre]\tError en socket(2)");

    /* Inicializa la estructura para la dirección del socket */
    bzero((char *) &server_addr, sizeof(server_addr));
    /* Establece el tipo de socket a utilizar */
    server_addr.sun_family = AF_UNIX;
    /* Establecer la ruta del archivo de socket */
    strcpy(server_addr.sun_path, ARCHIVO);
    /* Determina la longitud de la dirección del socket de servidor */
    server_len = strlen(server_addr.sun_path) + sizeof(server_addr.sun_family);

    /* Asigna la dirección al socket */
    if(bind(server_sock_fd, (struct sockaddr *) &server_addr, server_len) < 0)
      error("[padre]\tError en bind(2)");

    /* Abre el socket para recibir nuevas conexiones */
    if(listen(server_sock_fd, BACKLOG) < 0)
      error("[padre]\tError en listen(2)");
    /* Determina el tamaño de la dirección del cliente */
    client_len = sizeof(client_addr);

    /* Acepta la conexión de un cliente y regresa el descriptor de archivos */
    if((client_sock_fd = accept(server_sock_fd, (struct sockaddr *) &client_addr, &client_len)) < 0)
      error("[padre]\tError en accept(2)");

    /* Imprime una línea de identificación */
    printf("[padre]\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "[padre]\t\tLeyendo líneas desde: %s\n", ARCHIVO);

    /* Lee BUF_LEN bytes desde el socket e imprime a STDOUT */
    bytes = read(client_sock_fd, buffer, BUF_LEN);
    write(STDOUT_FILENO, buffer, bytes);

    /* Imprime una línea de identificación */
    fprintf(stderr, "[padre]\t\tImprimiendo líneas en: %s\n", ARCHIVO);

    /* Escribe un mensaje de manera incremental en el archivo abierto */
    for(int offset=1 ; offset<=strlen(MENSAJE) ; offset++)
    {
      /* Inicializa el buffer con '\0' */
      bzero(buffer, BUF_LEN);
      /* Copia una parte de MENSAJE hasta offset */
      strncpy(buffer, MENSAJE, offset);
      /* Agrega un retorno de línea al final de la cadena */
      buffer[offset] = '\n';
      /* Escribe en el socket el bloque de caracteres buffer */
      write(client_sock_fd, buffer, BUF_LEN);
    }
    /* Escribe el fin de transmisión en el socket */
    write(client_sock_fd, EOT, sizeof(EOT));

    /* Forza la escritura en todos los archivos abiertos */
    fflush(NULL);
    /* Espera a que el proceso hijo termine */
    wait(NULL);
    /* Cierra los descriptores de archivo de los sockets */
    close(client_sock_fd);
    close(server_sock_fd);
    /* Borra el archivo del socket */
    unlink(ARCHIVO);
  }

  /* Código que ejecuta el proceso hijo */
  else /* pid == 0 */
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getppid();
    pid_t pid_hijo = getpid();

    /* Inicializa el búffer con '\0' */
    char buffer[BUF_LEN] = "";
    bzero(buffer, BUF_LEN);

    /* Inicializa la estructura para la dirección del socket */
    bzero((char *) &server_addr, sizeof(server_addr));
    /* Establece el tipo de socket a utilizar */
    server_addr.sun_family = AF_UNIX;
    /* Establecer la ruta del archivo de socket */
    strcpy(server_addr.sun_path, ARCHIVO);
    /* Determina la longitud de la dirección del socket de servidor */
    server_len = strlen(server_addr.sun_path) + sizeof(server_addr.sun_family);

    /* Espera a que el proceso padre termine de crear el socket */
    sleep(1);

    /* Crea el socket del cliente y obtiene el descriptor de archivos */
    if ((client_sock_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
      error("[hijo]\tError en socket(2)");

    /* Se conecta al servidor */
    if (connect(client_sock_fd, (struct sockaddr *) &server_addr, server_len) < 0)
      error("[hijo]\tError en connect(2)");

    /* Imprime una línea de identificación */
    printf("[hijo]\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "[hijo]\t\tLeyendo líneas desde: %s\n", ARCHIVO);

    /* Envía un mensaje al servidor */
    sprintf(buffer, "Mensaje desde el cliente con PID: %d\n", pid_hijo);
    write(client_sock_fd, buffer, strlen(buffer));
    /* Forza la escritura en todos los archivos abiertos */
    fflush(NULL);

    /* Imprime una línea de identificación */
    fprintf(stderr, "[hijo]\t\tImprimiendo líneas en: %s\n", ARCHIVO);

    /* Inicializa el buffer con '\0' */
    bzero(buffer, BUF_LEN);

    /* Lee línea a línea el contenido del archivo abierto */
    int i = 1;
    ssize_t n = read(client_sock_fd, buffer, BUF_LEN);
    while (n > 1)
    {
#ifdef SLEEP
      /* Inserta una pausa */
      sleep(1);
#endif
      /* Imprime el contenido de la línea que se guardó en el búffer */
      printf("[hijo]\tbuffer: (%d)\t%s", i, buffer);
      /* Limpia el buffer despues de usarlo */
      bzero(buffer, BUF_LEN);
      i++;
      /* Lee del socket el siguiente bloque de caracteres */
      n = read(client_sock_fd, buffer, BUF_LEN);

      /* Termina el ciclo de lectura si lee el caracter de fin de transmisión */
      if (strcmp(buffer, EOT) == 0)
        break;
    }

    /* Cierra el descriptor de archivo del socket */
    close(client_sock_fd);
  }

  return EXIT_SUCCESS;
}
