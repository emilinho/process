/* http://www.yolinux.com/TUTORIALS/ForkExecProcesses.html */

#include <stdio.h>
#include <unistd.h>

int main(void)
{
  /* Vector de argumentos */
  char *args[] = {"/bin/ls", "-r", "-t", "-l", (char *) 0 };

  /* Cargar un programa que no existe */
  /* execv("/bin/ls", args); */
  execv(args[0], args);
  /* execv("/bin/nah", args); */

  printf("Si usted ve este mensaje es que algo falló con exec(3) :S\n");
  printf("Adios mundo cruel\n");
  return 1;
}
