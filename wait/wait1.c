#include <stdlib.h>
#include <unistd.h>

int main(void)
{
  pid_t pid;
  pid = fork();
  if (pid>0)
  {
    system("sleep 60");
  }
  return 0;
}
