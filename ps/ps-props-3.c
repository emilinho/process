/* man 7 credentials */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char* argv[], char* envp[])
{
  pid_t pid = getpid();

  printf("Identificador de proceso\n");
  printf("getpid(2)	PID:	%d\n\n", getpid());

  printf("Identificador de proceso padre\n");
  printf("getppid(2)	PPID:	%d\n\n", getppid());

  printf("Identificador de sesión\n");
  printf("getsid(2)	SID:	%d\n\n", getsid(pid));

  printf("Identificador de grupo de procesos\n");
  printf("getpgrp(2)	PGRP:	%d\n\n", getpgrp());

  printf("Identificador de usuario\n");
  printf("getuid(2)	UID:	%d\n\n", getuid());

  printf("Identificador de grupo\n");
  printf("getgid(2)	GID:	%d\n\n", getgid());

  printf("Línea de comandos, argumentos: %d\n", argc);
  for (int i=0 ; i < argc ; i++)
    printf("\targv[%d]:	%s\n", i, argv[i]);

  printf("Variables de entorno:\n");
  for (int i=0 ; envp[i] != NULL ; i++)
    printf("\tenvp[%d]:	%s\n", i, envp[i]);

  getchar();
  return 0;
}
