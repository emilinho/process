#	Makefile.inc

# Variables auxiliares
SEP?=-
COLS?=$(shell echo "`tput cols` - 1" | bc)
MSG?=

# Imprime un separador utilizando perl o el programa printf
separator:
	@(which perl &>/dev/null && perl -e 'print("${SEP}" x ${COLS} . "\n")' || for i in {1..${COLS}} ; do printf '${SEP}' ; done ; echo '')

banner:
	printf "\t%s\n\n" "${MSG}"
