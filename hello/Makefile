#!/usr/bin/make -f
SHELL=/bin/bash

# Incluye funciones auxiliares
include $(CURDIR)/../Makefile.inc

# Banderas estrictas para el compilador de C
CFLAGS=-Wall --pedantic

# Programa que se compilará y probará
PROG?=hello

# Variables auxiliares
TARGET?=${PROG}

# Regla por defecto
.DEFAULT_GOAL := default

# Ejecuta todos los pasos
all:	default
default:	build strace-all

# Limpia los archivos de salida
clean:
	reset
	@$(MAKE) -s separator banner MSG="Limpia los archivos de salida"
	-rm -v ${PROG} *.static *.strip *.readelf *.strace

build:	${PROG}.c
	@$(MAKE) -s separator banner MSG="Compila y hace las pruebas a los binarios"
	for TARGET in ${PROG} ${PROG}.static ; \
	do \
	  $(MAKE) compile TARGET=$${TARGET} ; \
	done ;

# Compila el programa
compile:
	@$(MAKE) -s separator
ifeq (static, $(findstring static, ${TARGET}))
	@$(MAKE) -s banner MSG="Agrega la opción -static si el nombre del programa tiene el sufijo 'static'"
	$(CC) ${CFLAGS} -static -o ${TARGET} ${PROG}.c
else
	@$(MAKE) -s banner MSG="Compila el programa de manera normal"
	$(CC) ${CFLAGS} -o ${TARGET} ${PROG}.c
endif
	@$(MAKE) -s banner MSG="Analiza el programa para ver sus propiedades"
	cp -v ${TARGET} ${TARGET}.strip
	strip ${TARGET}.strip
	for BINARY in $${TARGET} $${TARGET}.strip ; \
	do \
	  $(MAKE) analyze TARGET=$${BINARY} ; \
	done ; \

analyze:
	@$(MAKE) -s separator banner MSG="Analiza el programa para visualizar las propiedades del binario"
	ls -l ${TARGET}
	file -s ${TARGET}
	-ldd ${TARGET}
	readelf -e ${TARGET} | tee ${TARGET}.readelf

# Ejecuta los binarios para ver las llamadas al sistema que utilizan
strace-all:	strace strace.static

strace:	${PROG}
	@$(MAKE) -s separator
	@$(MAKE) -s banner MSG="Ejecuta el programa con strace para ver las llamadas al sistema"
	strace -f -e trace=all ./${PROG} 2>&1 | tee ${PROG}.strace

strace.static: ${PROG}.static
	@$(MAKE) -s separator
	@$(MAKE) -s banner MSG="Ejecuta el programa con strace para ver las llamadas al sistema"
	strace -f -e trace=all ./${PROG}.static 2>&1 | tee ${PROG}.static.strace
