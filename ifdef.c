#include <stdio.h>

/******************************************************************************/

/*
  Controla cual version se va a compilar
  Descomentar solo UNO
*/
/* #define NO_ARGS_NO_ENV */
/* #define ARGS_NO_ENV */
#define ARGS_ENV

/**************************************/

#ifdef NO_ARGS_NO_ENV
/*
  Sin argumentos ni variables de entorno
  ./programa
*/

#define MAIN int main(void)
#endif /* NO_ARGS_NO_ENV */

/**************************************/

#ifdef ARGS_NO_ENV
/*
  Con argumentos, sin variables de entorno
  ./programa arg1 arg2 arg3
*/

#define MAIN int main(int argc, char* argv[])
#endif /* ARGS_NO_ENV */

/**************************************/

#ifdef ARGS_ENV
/*
  Con argumentos y variables de entorno
  VARIABLE=valor ./programa arg1 arg2 arg3
*/

#define MAIN int main(int argc, char* argv[], char* envp[])
#endif /* ARGS_ENV */

/******************************************************************************/

/* Utiliza el símbolo MAIN que se definió previamente */
#ifdef MAIN
MAIN
{
  #if defined ARGS_NO_ENV || defined ARGS_ENV
  printf("Línea de comandos, argumentos: %d\n", argc);
  for (int i=0 ; i < argc ; i++)
    printf("\targv[%d]:	%s\n", i, argv[i]);
  #endif /* ARGS_NO_ENV || ARGS_ENV */

  #ifdef ARGS_ENV
  printf("Variables de entorno:\n");
  for (int i=0 ; envp[i] != NULL ; i++)
    printf("\tenvp[%d]:	%s\n", i, envp[i]);
  #endif /* ARGS_ENV */

  return 0;
}
#endif /* MAIN */
