/* http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/create.html */
/*
PROGRAM  fork-01.c
This program illustrates the use of fork() and getpid() system
calls. Note that write() is used instead of printf() since the
latter is buffered while the former is not.
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#define MAX_COUNT 200
#define BUF_SIZE  100

int main(void)
{
  int i;
  char buf[BUF_SIZE];
  pid_t pid;

  fork();
  /* El proceso padre e hijo hacen lo mismo */
  pid = getpid();
  for (i = 1; i <= MAX_COUNT; i++)
  {
    sprintf(buf, "This line is from pid %d, value = %d\n", pid, i);
    write(1, buf, strlen(buf));
  }
  sleep(60);
  return 0;
}
