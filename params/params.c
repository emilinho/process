/* 
  Revisa si hay argumentos de linea de comandos
    ./prog Andres 123456789 Ingenieria

  Revisa si hay variables de entorno
    A=Andres  B=123456789  C=Ingenieria  ./prog

  Lee argumentos desde la entrada estandar
    ./prog < entrada.txt
*/

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <strings.h>

#define BUF_LEN 80

int main(int argc, char* argv[], char* envp[])
{
  /* Inicializa el búffer con '\0' */
  char buffer[BUF_LEN];
  bzero(buffer, BUF_LEN);

  /* Imprime los parámetros obtenidos por línea de comandos */
  printf("Línea de comandos:\n");
  for (int i=0 ; i < argc ; i++)
    printf("\targv[%d]:	%s\n", i, argv[i]);

  /* Imprime las variables de entorno definidas */
  printf("Variables de entorno:\n");
  for (int i=0 ; envp[i] != NULL ; i++)
    printf("\tenvp[%d]:	%s\n", i, envp[i]);

  /* Lee los parámetros desde STDIN */
  printf("Leyendo parámetros desde STDIN:\n");
  fprintf(stderr, "Finalizar con EOF (Ctrl+D)\n");
  while(fgets(buffer, BUF_LEN, stdin) != NULL)
  {
    printf("%s", buffer);
  }

  /* Duerme el proceso */
  printf("sleep()\n");
  fprintf(stderr, "Finalizar con SIGINT (Ctrl+C)\n");
  sleep(UINT_MAX);

  return 0;
}
